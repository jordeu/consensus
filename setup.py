from os import path
from setuptools import setup, find_packages
from consensus import __version__


directory = path.dirname(path.abspath(__file__))
with open(path.join(directory, 'requirements.txt')) as f:
    required = f.read().splitlines()


setup(
    name='consensus',
    version=__version__,
    description='Consensus ranking based on Schulze voting method',
    packages=find_packages(),
    package_data={'consensus': ['*.pyx']},
    author='Ferran Mui�os',
    author_email='ferran.muinos@irbbarcelona.org',
    install_requires=required,
    license='GNU GPLv3',
    url='https://bitbucket.org/ferran_muinos/consensus',
)
