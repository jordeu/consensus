README
======

My name is **consensus**. I am a Python package.

I can carry out ranking combinations using Schulze voting method.

Schulze method is described in this `paper <https://link.springer.com/article/10.1007%2Fs00355-010-0475-4>`_.

I can handle the following computations:

+ generate random ballots
+ compute consensus ranking with Schulze method
+ do so by tuning the voting rights of voters
+ plot weight graphs
+ conduct enrichment-based voting rights optimization

Refer to this `presentation <http://slides.com/ferranmuinos/deck/fullscreen>`_ for a brief motivation to the methodology.


Install
-------

Clone with git

.. code::

   $ git clone https://bitbucket.org/ferran_muinos/consensus

or 

download from the `downloads page <https://bitbucket.org/ferran_muinos/consensus/downloads/>`_.

Move to the top folder of the repo and install with pip:

.. code::
	
	$ pip install .


Repo tree
---------

The repo has the following structure::

   |- consensus/
   |
   |  |- consensus/
   |  |  |
   |  |  |- __init__.py
   |  |  |- main.py
   |  |  |- schulze.py
   |  |  |- schulze_election.py
   |  |  |- schulze_election.py
   |  |  |- schulze_strongest_path.py
   |  |  |- schulze_strongest_path_cython.pyx
   |  |  |- grid_optimizer.py
   |  |  |- utils.py
   |
   |- consensus_notebook.ipynb
   |- .gitignore
   |- LICENSE
   |- MANIFEST.in
   |- README.rst
   |- requirements.txt
   |- setup.py


Usage example
-------------

The notebook `consensus_notebook.ipynb <http://nbviewer.jupyter.org/urls/bitbucket.org/ferran_muinos/consensus/raw/master/consensus_notebook.ipynb>`_ 
provides a standard example of usage.