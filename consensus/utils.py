import numpy as np
import networkx as nx


def single_random_ballot(n):
    queue = list(np.random.choice(np.arange(n), size=n, replace=False))  # queue = candidates shuffled
    poset = np.random.randint(0, 2, size=n-1)  # encode whether two adjacent candidates have same rank
    ballot = [None for i in range(n)]  # initialize ballot
    rank = 1  # initialize rank
    count = 0  # initialize count
    ballot[queue.pop(0)] = rank
    while queue:
        if poset[count] == 0:
            rank = count + 2  # update rank
        ballot[queue.pop(0)] = rank
        count += 1
    return ballot


def generate_ballots(voters, candidates):
    n = len(candidates)
    ballot = {}
    for v in voters:
        ballot[v] = single_random_ballot(n)
    return ballot


def weight_graph(w_matrix, candidates, option=1):
    G = nx.DiGraph()
    edge_labels_dict = {}
    for i in range(w_matrix.shape[0]):
        for j in range(w_matrix.shape[1]):
            if j > i:
                if option * (w_matrix[i, j] - w_matrix[j, i]) > 0:
                    G.add_edge(candidates[i], candidates[j])
                    edge_labels_dict[(candidates[i], candidates[j])] = w_matrix[i, j]
                elif w_matrix[i, j] == w_matrix[j, i]:
                    G.add_edge(candidates[i], candidates[j])
                    G.add_edge(candidates[j], candidates[i])
                    edge_labels_dict[(candidates[i], candidates[j])] = w_matrix[i, j]
                    edge_labels_dict[(candidates[j], candidates[i])] = w_matrix[j, i]
                elif option * (w_matrix[i, j] - w_matrix[j, i]) < 0:
                    G.add_edge(candidates[j], candidates[i])
                    edge_labels_dict[(candidates[j], candidates[i])] = w_matrix[j, i]
    return G, edge_labels_dict
