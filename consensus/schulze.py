import gzip
import json
import click

import summary
from consensus.schulze_election import combination_ranking


def combination(d_results, weights=None):
    """
    Generate the optimized ranking by the weights calculated by the opmitzer
    :param weights: dict of weights
    :param d_results: dictionary of results of the individual methods
    """

    if weights is None:
        weights = {}
        for method in d_results:
            weights[method] = 1.
    ranking = combination_ranking(d_results, weights)
    return summary.output_to_dataframe(ranking, d_results)


@click.command()
@click.option('--rankings', type=click.Path(exists=True), help="json dict with rankings of individual methods")
@click.option('--weights', type=click.Path(), help="json dict weights")
@click.option('--output', type=click.Path(), help="path output dataframe")
def run_schulze(input_data, weights, output):

    with gzip.open(input_data, "rb") as fd:
        d_results = json.load(fd)

    if weights is not None:
        with gzip.open(weights, "rb") as fd:
            weights = json.load(fd)

    df = combination(d_results, weights=weights)
    df.to_csv(output, sep='\t', index=False)

if __name__ == '__main__':
    run_schulze()
