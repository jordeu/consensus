import os
import gzip
import click
import pickle
import numpy as np
import pandas as pd
import itertools
from tqdm import tqdm
from scipy.optimize import minimize
from scipy.optimize import basinhopping
from functools import partial
from schulze_election import combination_ranking


VOTERS = ['v1', 'v2', 'v3', 'v4', 'v5','v6']


def calculate_objective_function(ballots, weights, enrichment_score=None):
    """
    Given rankings produced by voters with weights, returns the score
    :param weights: dictionary of the weights where the keys are the
                    methods and the values are the current distribution of weights.
    :param ranking: dictionary of ranking of the methods where the keys are the
                    cancer types and the methods per cancer type.
    :param enrichment_score: function: ranking -> R
    :return float of the current value of the objective function for the input weights.
    """

    ranking = combination_ranking(ballots, weights=weights)
    return enrichment_score(ranking)
    return d_area[objective_method]


# constraint function wrappers


def lower_bound(w, i):
    # set a reasonable lower bound for individual voting rights
    return w[i] - 0.1


def upper_bound(w, i):
    # set a reasonable upper bound for individual voting rights
    return - w[i] + 0.5


def simplex_bound(w):
    # set the scale of voting rights so that they add up to 1
    return sum(w) - 1


def arbitrary_bound_1(w):
    return - sum([w[i] for i in range(len(VOTERS)) if i % 2 == 0]) + 0.5


def arbitrary_bound_2(w):
    return - sum([w[i] for i in range(len(VOTERS)) if i % 2 == 1]) + 0.5


def grid_optimize(func, low_quality=set()):
    """
    :param: func: function to be optimized
    :return: best candidates
    """
    
    optimum = {v: None for v in VOTERS}
    optimum.update({'objective': 0})
    methods_list = ['oncodriveclustl_r', 'dndscv_r', 'oncodrivefml_r', 'hotmapssignature_r', 'edriver_r', 'cbase_r']
    for w in itertools.product(np.linspace(0, 1, 21), repeat=len(VOTERS)-1):
        if sum(w) <= 0.95:  # belongs to simplex
            w = np.append(w, [1 - sum(w)]).tolist()
            if (low[0] <= w[0] < 0.5) and (low[3] <= w[3] < 0.5) and (low[4] <= w[4] < 0.5):
                if w[0] + w[3] + w[4] < 0.5:  # cluster constraint
                    if low[2] <= w[2] < 0.5:    # fm bias constraint
                        if (low[1] <= w[1] < 0.5) and (low[5] <= w[5] < 0.5):
                            if w[1] + w[5] < 0.5:  # recurrence constraint
                                f = func(w)
                                if optimum['Objective_Function'] > f:
                                    optimum['Objective_Function'] = f
                                    optimum['oncodriveclustl_r'] = w[0]
                                    optimum['dndscv_r'] = w[1]
                                    optimum['oncodrivefml_r'] = w[2]
                                    optimum['hotmapssignature_r'] = w[3]
                                    optimum['edriver_r'] = w[4]
                                    optimum['cbase_r'] = w[5]
    return optimum


def array_component(w, i):
    return w[i]


def create_scipy_constraints(low_quality=set()):
    """Create the constraints for the optimization process"""

    cons = [{'type': 'eq', 'fun': simplex_bound},
            {'type': 'ineq', 'fun': arbitrary_bound_1},
            {'type': 'ineq', 'fun': arbitrary_bound_2}]
    for ind, v in enumerate(VOTERS):
        cons += [{'type': 'ineq', 'fun': partial(lower_bound, i=ind)}]
        cons += [{'type': 'ineq', 'fun': partial(upper_bound, i=ind)}]
    return tuple(cons)


def optimize_with_seed(func, w0, low_quality=set()):
    """
    Args:
        func: function admitting w as argument
        w0: array: array of weights
        low_quality: list of methods for which weight shall be 0.0.
    Returns:
        array of weights that minimizes function
    """

    cons = create_scipy_constraints(low_quality=low_quality)
    niter = 25
    epsilon = 0.02
    options = {'maxiter': niter, 'eps': epsilon, 'ftol': 1e-3, 'disp': True}
    minimizer_kwargs = {'method': 'SLSQP', 'options': options, 'constraints': cons}
    res = basinhopping(func, w0, minimizer_kwargs=minimizer_kwargs, niter=1, stepsize=0.05)
    return res



def full_optimizer(cancer, input_rankings, method_reject, moutput, percentage_cgc, seed, t_combination):

    global gavaliable_methods, order_methods, gt_combination
    if seed == 'T':
        np.random.seed(1)
    # Select order methods
    gt_combination = t_combination
    if t_combination == "RANKING":
        order_methods = order_methods_ranking
    else:
        order_methods = order_methods_threshold
    print(input_rankings)
    with gzip.open(input_rankings, "rb") as fd:
        d_results_methodsr = pickle.load(fd)
    print(d_results_methodsr.keys())
    # Prepare the list of available methods and the dictionary of weights
    l = []
    for method in d_results_methodsr.keys():
        l.append(method)
    gavaliable_methods = list(l)
    # Remove methods that do not reach the quality metrics
    if not (method_reject is None):
        discarded = set()
        discarded.add(method_reject + "_r")
    else:
        discarded = set(["{}_r".format(m) for m in Filter(input_run=moutput, tumor=cancer).filters()])
    # Include discarded from command line
    if len(discarded) > 0:
        print("[QC] {} discarded {}".format(cancer, discarded))
    gavaliable_methods = [m for m in gavaliable_methods if m not in discarded]
    print("Running on " + str(gavaliable_methods))
    # Set to empty those methods discarded or not present
    for method in order_methods:
        if method not in gavaliable_methods:
            d_results_methodsr[method] = {}
    print(d_results_methodsr)
    # Instantiate the enrichment objective function
    objective_function = Evaluation_Enrichment(percentage_cgc)
    f = partial(calculate_objective_function, d_results_methodsr, objective_function=objective_function)
    if t_combination == "RANKING":
        def func(w):
            return -f({"oncodriveclustl_r": w[0],
                       "dndscv_r": w[1],
                       "oncodrivefml_r": w[2],
                       "hotmapssignature_r": w[3],
                       "edriver_r": w[4],
                       "cbase_r": w[5]})
    else:
        def func(w):
            return -f({"oncodriveclustl_t": w[0],
                       "dndscv_r": w[1],
                       "oncodrivefml_t": w[2],
                       "hotmapssignature_t": w[3],
                       "edriver_t": w[4],
                       "cbase_t": w[5]})
    all_methods = ['oncodriveclustl_r', 'dndscv_r', 'oncodrivefml_r', 'hotmapssignature_r', 'edriver_r', 'cbase_r']
    # best solution in 1/20 resolution grid, augmented with basin-hopping/SLSQP optimization
    grid_optimum = grid_optimize(func, low_quality=discarded)  # get optimum candidate in the grid
    w = np.array([grid_optimum[k] for k in all_methods])
    res = optimize_with_seed(func, w)  # basin-hopping/SLSQP using grid optimum candidate as initial guess
    res_dict = dict(zip(all_methods, list(res.x)))
    res_dict['Objective_Function'] = res.fun
    # choose the best one grid or basin-hopping, unless basin-hopping does not fulfill the constraints
    if res_dict['Objective_Function'] > grid_optimum['Objective_Function']:
        out_df = pd.DataFrame({k: [v] for k, v in grid_optimum.items()})
    else:
        r = np.array([res_dict[k] for k in all_methods])
        if satisfy_constraints(r, low_quality=discarded):
            out_df = pd.DataFrame({k: [v] for k, v in res_dict.items()})
        else:
            out_df = pd.DataFrame({k: [v] for k, v in grid_optimum.items()})
    return out_df


def skip_optimizer(input_rankings, method_reject, moutput, cancer):

    global gavaliable_methods

    gavaliable_methods = ['oncodriveclustl_r', 'dndscv_r', 'oncodrivefml_r', 'hotmapssignature_r', 'edriver_r', 'cbase_r']

    # Remove methods that do not reach the quality metrics
    if not (method_reject is None):
        discarded = set()
        discarded.add(method_reject + "_r")
    else:
        discarded = set(["{}_r".format(m) for m in Filter(input_run=moutput, tumor=cancer).filters()])
    # Include discarded from command line
    gavaliable_methods = [m for m in gavaliable_methods if m not in discarded]
    print("Running on " + str(gavaliable_methods))
    # Create a uniform vector of weights
    N = len(gavaliable_methods)
    df = pd.DataFrame({k: [1/N] for k in gavaliable_methods})
    if len(discarded) > 0:
        print("[QC] {} discarded {}".format(cancer, discarded))
        for md in discarded:
            df[md] = 0.0
    df["Objective_Function"] = np.nan
    return df



@click.command()
@click.option('--foutput',
              type=click.Path(),
              help="File of output",
              required=True)
@click.option('--input_rankings',
              type=click.Path(exists=True),
              help="Dictionary with the ranking of the methods",
              required=True)
@click.option('--t_combination',
              default="RANKING",
              help='Type of combination, ranking or threshold. Default: "RANKING"',
              required=True)
@click.option('--percentage_cgc',
              default=1.0,
              help='Percentage of CGC used in the optimization. Default all.')
@click.option('--moutput',
              type=click.Path(exists=True),
              help="Input data directory",
              required=True)
@click.option('--method_reject',
               type=click.STRING,
               help="Method for which weight in combination is forced to be zero",
               required=False)
@click.option('--cancer',
              type=str,
              required=True)
@click.option('--seed',
              default="T",
              type=str,
              help="Whether a seed for random generation should be defined. Default T=True. [T=True,F=False]",
              required=True)


def run_optimizer( foutput, input_rankings, t_combination, percentage_cgc, moutput, cancer, seed, method_reject):

    if float(percentage_cgc) > 0.0:
        out_df = full_optimizer(cancer, input_rankings, method_reject, moutput, percentage_cgc, seed, t_combination)
    else:
        out_df = skip_optimizer(input_rankings,method_reject,moutput,cancer)
    # write to table
    out_df.to_csv(foutput, sep="\t", index=False, compression="gzip")


if __name__ == '__main__':

    run_optimizer()
